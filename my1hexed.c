/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <ctype.h>
/*----------------------------------------------------------------------------*/
/* this will make off_t 64-bits */
#define _FILE_OFFSET_BITS 64
/*----------------------------------------------------------------------------*/
/* this will enable ftello and fseeko - glibc thingy? */
#define _LARGEFILE_SOURCE 1
/*----------------------------------------------------------------------------*/
#define SECTOR_SIZE 512
/*----------------------------------------------------------------------------*/
#define PATH_SEP '/'
/*----------------------------------------------------------------------------*/
void show_header(char* name, off_t size, int* help)
{
	printf("\n\n");
	if(*help)
	{
		printf("Viewer Commands:\n");
		printf("<G>o [#] <S>ector [#] <C>hange [#] {#} "
			"<N>ext <P>revious <Q>uit\n\n");
		*help = 0;
	}
	else
	{
		printf("FileName: '%s'\n",name);
		printf("Size: %ld byte(s)\n\n",size);
	}
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	char* basename(char* pname, int nodot);
	char buff[16]; /* line buffer */
	int cols, rows;
	int test, loop, help;
	off_t size, init, addr;
	FILE *pfile;
	putchar('\n');
	if(argc!=2)
	{
		printf("View a file in HEX mode\n\n");
		basename(argv[0],1);
		printf("Usage: %s <filename>\n\n",argv[0]);
	}
	else
	{
		cols=0; rows=0;
		init=0; addr=0; help = 0;
		pfile = fopen(argv[1],"rb+");
		if(pfile)
		{
			/* remove path from filename */
			basename(argv[1],0);
			/* try to get file size */
			fseeko(pfile,0L,SEEK_END);
			size = ftello(pfile);
			/* reset file pointer position */
			fseeko(pfile,0L,SEEK_SET);
			/* show header */
			show_header(argv[1],size,&help);
			/* loop through */
			while(1)
			{
				/* show address @start of line */
				if(cols==0)
				{
					char format[] = "%12x - ";
					/* prepare address formatting */
					if (addr<0x100000000)
					{
						format[1] = '0';
						format[2] = '8';
					}
					if(addr<size) printf(format,addr);
					else if (addr<0x100000000) printf("xxxxxxxx - ");
					else printf("xxxxxxxxxxxx - ");
				}
				if(addr<size)
				{
					test = fgetc(pfile);
					addr++;
				}
				else test = 0;
				/* data in hex - if no data, just show zero! */
				printf("%02x ",test);
				buff[cols] = (char) test;
				cols++;
				init++;
				/* check maximum line buffer */
				if(cols==16)
				{
					printf("- ");
					/* show data in ascii - '.' if non-printable */
					for(loop=0;loop<16;loop++)
					{
						if(isprint(buff[loop]))
							putchar(buff[loop]);
						else
							putchar('.');
					}
					putchar('\n');
					rows++;
					 /* check max line per page */
					if(rows==20)
					{
						/* show command prompt */
						off_t bidx;
						char tbuf[40];
						int bval, done = 0, stay = 1;
						printf("\nCommand (h=help): ");
						fgets(tbuf,40,stdin);
						/* command options */
						switch(tbuf[0])
						{
						case 'H':
						case 'h':
							help = 1;
							break;
						case 'C':
						case 'c':
							/** get location and value */
							if(sscanf(&tbuf[1],"%lx %x",&bidx,&bval)==2)
							{
								if(bidx>=0&&bidx<size&&bval>=0&&bval<256)
								{
									fseeko(pfile,bidx,SEEK_SET);
									fputc((unsigned char)bval,pfile);
									stay = 1;
								}
							}
							break;
						case 'G':
						case 'g':
							/** get location */
							if(sscanf(&tbuf[1],"%lx",&bidx)==1)
							{
								if(bidx>=0&&bidx<size)
								{
									bidx -= bidx%16;
									fseeko(pfile,bidx,SEEK_SET);
									stay = 0;
									addr = bidx;
									init = addr;
								}
							}
							break;
						case 'S':
						case 's':
							/** get sector */
							if(sscanf(&tbuf[1],"%ld",&bidx)==1)
							{
								bidx *= SECTOR_SIZE;
								if(bidx>=0&&bidx<size)
								{
									bidx -= bidx%16;
									fseeko(pfile,bidx,SEEK_SET);
									stay = 0;
									addr = bidx;
									init = addr;
								}
							}
							break;
						case 'N':
						case 'n':
							if(addr==init)
								stay = 0;
							break;
						case 'P':
						case 'p':
							if(init>=640)
							{
								init -= 640;
								addr -= 640;
								if(addr!=init)
								{
									fseeko(pfile,init,SEEK_SET);
									addr = init;
								}
								else
									fseeko(pfile,-640L,SEEK_CUR);
								stay = 0;
							}
							break;
						case 'Q':
						case 'q':
							done = 1;
							stay = 0;
							break;
						}
						if(stay)
						{
							init -= 320;
							if(addr!=init)
								fseeko(pfile,init,SEEK_SET);
							else
								fseeko(pfile,320L,SEEK_SET);
							addr = init;
						 }
						if(done) break;
						show_header(argv[1],size,&help);
						rows = 0;
					}
					cols = 0;
				}
			}
			fclose(pfile);
		}
		else
		{
			printf("Error opening file '%s'!\n",argv[1]);
		}
	}
	putchar('\n');
	return 0;
}
/*----------------------------------------------------------------------------*/
char* basename(char* pname, int nodot)
{
	int index = -1, index_d = -1, size = -1, step = 0, loop;
	while(pname[step]!='\0') step++;
	for(loop=step-1;loop>=0;loop--)
	{
		if(pname[loop]=='.')
		{
			if(index_d<0) index_d = loop;
		}
		if(pname[loop]==PATH_SEP)
		{
			index = loop + 1;
			size = step - index;
			if(nodot&&index_d>=0)
				size -= step - index_d;
			break;
		}
	}
	if(index>=0)
	{
		for(loop=0;loop<size;loop++)
		{
			pname[loop] = pname[loop+index];
		}
		pname[size] = '\0';
	}
	return pname;
}
/*----------------------------------------------------------------------------*/
