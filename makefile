# A simple makefile for my1hexed

CC  = gcc
RM = rm -f

PROGRAM = my1hexed
OBJECTS = $(PROGRAM).o

CFLAG = -c -Wall
LFLAG = -s -static

all: ${PROGRAM}

new: clean all

${PROGRAM}: $(OBJECTS)
	${CC} ${LFLAG} -o $@ $^

.c.o:
	${CC} ${CFLAG} -o $@ $<

clean:
	-$(RM) ${PROGRAM} *.o *.obj *.exe
